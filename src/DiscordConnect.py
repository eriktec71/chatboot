import os
import discord
import db
from dotenv import load_dotenv
from sentiment_analysis_spanish import sentiment_analysis

load_dotenv()

TOKEN = os.getenv("DISCORD_TOKEN")

client = discord.Client()

connection = db.DB()
#mysql = connection.conexion()
#connection.createDB(mysql)
#mysql = NUL

#mysql = connection.conexionBaseDatos()
#connection.createTable(mysql)

sentiment = sentiment_analysis.SentimentAnalysisSpanish()

@client.event
async def on_ready():
  print(f'{client.user} has connected Discord!!')

@client.event
async def on_message(message):
  if message.author == client.user:
    return
  response = sentiment.sentiment(message.content)
  await message.channel.send(response)
  

client.run(TOKEN)