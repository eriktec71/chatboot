from http import server
from sqlite3 import Connection
import mysql.connector
import os
from dotenv import load_dotenv


class DB:

  load_dotenv()

  def __init__(self):
    self.__HOST = os.getenv("DB_HOST")
    self.__USER = os.getenv("DB_USER")
    self.__PASS = os.getenv("DB_PASSWORD")

  def conexion(self):
    return mysql.connector.connect(host=self.__HOST, user=self.__USER, password=self.__PASS)

  def conexionBaseDatos(self):
    return mysql.connector.connect(host=self.__HOST, user=self.__USER, password=self.__PASS, database="chatboot")

  def createDB(self, mydb):
    print("creando base de datos")
    vmx = mydb.cursor()
    vmx.execute("CREATE DATABASE chatboot")
    print("Base de datos creada")

  def createTable(self, mydb):
    print("Creando Tabla")
    vmx = mydb.cursor()
    vmx.execute("CREATE TABLE Mensajes (MSG VARCHAR(255), SENTIMIENTO VARCHAR(255), PROBABILIDAD FLOAT(10))")
    print("Tabla creada")